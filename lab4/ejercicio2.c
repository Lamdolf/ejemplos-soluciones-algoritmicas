#include <stdio.h>


void raiz_digital(int numero, int raiz){

    if(numero != 0){
        raiz = raiz + numero%10;
        numero = numero/10;
        raiz_digital(numero, raiz);
    }

    else if(raiz > 9){
        raiz_digital(raiz, 0);
    }

    else{
        printf("La raiz digital de su numero es: %d", raiz);
    }
}

int main(){

    int numero;

    printf("Por favor, ingrese un numero entero: ");
    scanf("%d", &numero);

    raiz_digital(numero, 0);

    return 0;
}
