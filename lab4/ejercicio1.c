#include <stdio.h>

void ulam(int numero){

    printf("%d ", numero);

    if(numero%2 == 1 && numero != 1){
        ulam((numero*3)+1);
    }
    else if(numero != 1){
        ulam(numero/2);
    }
}


int main(){

    int numero;

    printf("Ingrese un numero entero positivo: ");
    scanf("%d", &numero);

    if(numero <= 0){
        printf("Numero no valido\n");
        main();
    }
    ulam(numero);

    return 0;
}
