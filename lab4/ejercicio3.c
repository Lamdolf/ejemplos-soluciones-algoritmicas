#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int aleatorio(int opcion){

    if(opcion == 1){
        return (rand()%36)+1;
    }
    else{
        return (rand()%2)+1;
    }
}


void ruleta(int saldo){

    int apuesta, numero, color, numero_ganador, color_ganador;
    char eleccion;

    printf("Su saldo es: %d\n", saldo);
    printf("¿Cuanto desea apostar? ");
    scanf("%d", &apuesta);

    if(apuesta > saldo || apuesta < 0){
		system("clear");
        printf("Apuesta no valida\n");
        ruleta(saldo);
    }

    while(1){
        printf("\n¿A que numero desea apostar (1-36)? ");
        scanf("%d", &numero);

        if(numero > 0 && numero <37){
            break;
        }
        else{
            printf("Numero no valido\n");
        }
    }

    while(1){
        printf("\nA que color desea apostar? (1) negro, (2) rojo");
        scanf("%d", &color);

        if(color != 1 && color != 2){
            printf("Color no valido\n");
        }
        else{
            break;
        }
    }

    numero_ganador = aleatorio(1);
    color_ganador = aleatorio(2);

    if(numero == numero_ganador && color == color_ganador){
        printf("\n Has ganado!!\n");
        saldo = saldo + (36*apuesta);
    }

    else{
        printf("\nSalio el numero %d y el color %d, el jugador ha perdido\n", numero_ganador, color_ganador);
        saldo = saldo - apuesta;
    }

    while(1){
        printf("¿Desea seguir jugando? (s/n)");
        scanf(" %c", &eleccion);

        if(eleccion == 's' || eleccion == 'S'){
			system("clear");
            ruleta(saldo);
        }
        else if(eleccion == 'n' || eleccion == 'N'){
            system("exit");
            break;
        }
        else{
            printf("Eleccion no valida\n");
        }
    }
}


int main(){

    int saldo;

    printf("Ruleta ICB\n");
    printf("¿Cuanto es su monto inicial? ");
    scanf("%d", &saldo);

    srand(time(0));
    ruleta(saldo);

    return 0;
}
