{
    #Solo se leen las líneas cuya primera columna se "ATOM"
    if ($1 == "ATOM"){

        #Si por alguna razón la columna de residuo tiene 4 letras o la
        #columna con el nombre del elemento actual tiene 4 letras,
        #se compensa el error con substr en ambos casos
        if (length($3) > 3){
            print  substr($3,0,4), substr($3,5,3), $4, $5, $6, $7, $8
        }


        else if (length($4) > 3){
            print $3, substr($4,2,3), $5, $6, $7, $8, $9
        }

        #Si ninguna de las condiciones anteriores ocurren, se capturan
        #los siguientes datos útiles:
        #$3 = nombre del elemento
        #$4 = nombre del residuo
        #$5 = cadena del residuo
        #$6 = número de residuo
        else{
            print $3, $4, $5, $6, $7, $8, $9
        }
    }
}
