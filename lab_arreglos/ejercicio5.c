#include <stdio.h>


//Función que revisa si un número es primo
int primo(int numero){

    int primo, multiplos;
    primo = 1; //Booleano
    multiplos = 1;

    //Si tiene mas de 2 múltiplos, no es primo
    for(int i=2; i<=numero; i++){

        if(numero%i == 0){
            multiplos++;
        }
    }

    if(multiplos > 2){
        primo = 0;
    }

    //Se retorna un booleano
    return primo;
}


void arreglo(){

    int aux = 0; //Variable aux se utilizará varias veces

    //Se revisa la cantidad de primos que existen entre 2 y 100 (1 no cuenta)
    for(int i=2; i<=100; i++){

        //Cuando se usan booleanos, se considera el 1 como "Si se cumple" o "True"
        //Mientras que el 0 se considera como "No se cumple" o "False"
        //Y no es necesario especificar si no es igual a 1 o 0
        if(primo(i)){
            aux++;
        }
    }

    //Se crea un arreglo con largo de la cantidad de primos
    int arreglo[aux];

    aux = 0; //Se devuelve a 0 para usarla como contador nuevamente

    //Se llena el arreglo solo con número primos
    for(int i=2; i<=100; i++){

        if(primo(i)){
            arreglo[aux] = i;
            aux++;
        }
    }

    //Se imprime el arreglo de números primos
    printf("Su arreglo solo con número primos (en orden descendente) es:\n");
    for(int i=aux-1; i>=0; i--){
        printf("| %d |", arreglo[i]);
    }
    printf("\n");

}


int main(){

    arreglo();

    return 0;
}
