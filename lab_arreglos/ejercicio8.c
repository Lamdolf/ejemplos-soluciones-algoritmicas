#include <stdio.h>


void comparar_arreglos(int largo){

    int arreglo_1[largo], arreglo_2[largo];


    //Se usan dos bucles juntos para llenar ambos arreglos, el primero solo
    //cambia el arreglo que se esta llenando, mientras que el segundo llena
    //cada elemento del arreglo
    for(int i=1; i<=2; i++){
        for(int j=0; j<largo; j++){

            printf("Ingrese valor para el elemento %d del arreglo %d: ", j+1, i);

            if(i == 1){
                scanf("%d", &arreglo_1[j]);
            }
            else{
                scanf("%d", &arreglo_2[j]);
            }
        }
        printf("\n#######################################\n\n"); //Estética
    }

    int diferencias = 0;

    printf("Sus arreglos son: \n");
    printf("Arreglo 1  |  Arreglo 2\n");

    //Se imprimen ambos arreglos al mismo tiempo que se cuenta la cantidad de
    //diferencias entre ambos arreglos
    for(int i=0; i<largo; i++){
        printf("    %d     -     %d\n", arreglo_1[i], arreglo_2[i]);
        if(arreglo_1[i] != arreglo_2[i]){
            diferencias++;
        }
    }


    //Se dice que tan parecidos son ambos arreglos
    if(diferencias == 0){
        printf("Son idénticos\n");
    }
    else if(diferencias <= 2){
        printf("Son casi idénticos\n");
    }
    else if(diferencias <=5){
        printf("No son ni tan parecidos\n");
    }
    else{
        printf("No se parecen\n");
    }
}

int main(){

    int largo;

    printf("Ingrese largo para dos arreglos: ");
    scanf("%d", &largo);

    comparar_arreglos(largo);

    return 0;
}
