#include <stdio.h>


void llenar_y_acumular(int largo){

    int array[largo], array_acumulado[largo];

    //Se llena el primer arreglo a mano
    for (int i=0; i<largo; i++){

        printf("Ingrese un valor para el elemento %d del arreglo: ", i+1);
        scanf("%d", &array[i]);
    }

    //Se le asigna un valor inicial al primer elemento del arreglo acumulado
    array_acumulado[0] = array[0];


    //Se imprime el arreglo acumulado al mismo tiempo que se genera
    printf("Su arreglo acumulado es:\n| %d |", array_acumulado[0]);
    for (int i=1; i<largo; i++){
        array_acumulado[i] = array[i] + array_acumulado[i-1];
        printf("| %d |", array_acumulado[i]);
    }
    printf("\n");
}


int main(){

    int len;

    printf("Ingrese largo del arreglo que desea crear: ");
    scanf("%d", &len);

    llenar_y_acumular(len);

    return 0;
}
