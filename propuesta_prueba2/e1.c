#include <stdio.h>

int main(){

    char frase[99];
    char mayusculas[27] = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    char minusculas[27] = "abcdefghijklmnñopqrstuvwxyz";
    int aux = 1, especial = 0;

    printf("Ingrese frase: ");
    scanf("%[^\n]s", frase); //scanf que capta espacios

    for (int i = 0; frase[i] != '\0'; i++){
        especial = 1; // auxiliar para caracteres especiales

        for (int j = 0; j<27; j++){

            if (frase[i] == minusculas[j]){

                //Si se encontró un espacio, se imprime la letra mayúscula
                if (aux){
                    printf("%c", mayusculas[j]);
                    aux = 0;
                }

                //Si no, solo se imprime en minúscula
                else{
                    printf("%c", minusculas[j]);
                }
                //Dado que se imprimió una letra, se cambia el booleano de
                //caracter especial a 0
                especial = 0;
            }

            //Si el caracter es un espacio, se deja la variable auxiliar en
            //valor "true" o "verdadero"
            else if (frase[i] == 32){
                printf(" ");
                aux = 1;
                especial = 0;
                break;
            }
        }

        //Si no se imprimió una letra ni un espacio, entonces se asume que
        //es un caracter especial y se imprime simplemente
        if (especial){
            printf("%c", frase[i]);
        }
    }

    return 0;
}
