#include <stdio.h>
#include <stdlib.h>
#include <time.h>


//Función que imprime números afortunados si es que existen
void end(int n, int arreglo[n]){

    printf("Los numeros afortunados son: ");

    for (int i=0; arreglo[i] != '\0'; i++){
        printf("%d ", arreglo[i]);
    }
}

//Función que utiliza adaptación de ordenamiento de burbuja para desplazar
//todos los números a la izquierda del arreglo y los espacios vacíos a la
//derecha
void ordenar(int n, int arreglo[n]){

    int aux;

    for (int j=0; j<n-1; j++){
        for(int k=0; k<n-1; k++){

            if(arreglo[k] == '\0'){
                aux = arreglo[k];
                arreglo[k] = arreglo[k+1];
                arreglo[k+1] = aux;
            }
        }
    }

}

//Función que imprime el arreglo en su estado actual (si es que se cambió)
void imprimir(int n, int arreglo[n]){

    printf("\n");
    for (int i=0; arreglo[i] != '\0'; i++){
        printf("%d ", arreglo[i]);
    }
    printf("\n");
}


int main(){

    int n, indices;
    srand(time(0));

    printf("Ingrese un numero N para generar arreglo: ");
    scanf("%d", &n);

    int arreglo[n];

    //Llenado inicial del arreglo de tamaño n con números desde 1 hasta n
    for (int i=0; i<n; i++){
        arreglo[i] = (rand()%n)+1;
        printf("%d ", arreglo[i]);
    }
    printf("\n");

    for (int i=0; i<n; i++){

        //Se guarda el número del índice actual para evitar errores
        indices = arreglo[i];

        //Si el índice del primer multiplo del número seleccionado esta vacío
        //se termina el programa con los números afortunados
        if(arreglo[arreglo[i]-1] == '\0'){
            end(n, arreglo);
            break;
        }

        //Si el multiplo es 1, eliminará todo, por lo que no hay afortunados
        else if(arreglo[i] == 1){
            printf("No existen numeros afortunados");
            break;
        }

        //Si el índice actual esta vacío, se entregan los números afortunados
        else if (arreglo[i] == '\0'){
            end(n, arreglo);
            break;
        }

        //Si no se debe terminar, se revisa que elementos deben eliminarse
        else{
            //Con ayuda de un ciclo, se averígua que elementos deben eliminarse
            for (int j=0; j<n; j++){

                //Si el elemento actual es multiplo del valor actual, se elimina
                //ese elemento del arreglo
                if((j+1)%indices == 0){
                    arreglo[j] = '\0';
                }
            }
        }

        //Antes de cada nueva revisión, se ordena el arreglo y se imprime para
        //su visualización.
        ordenar(n, arreglo);
        imprimir(n, arreglo);
    }

    return 0;
}
