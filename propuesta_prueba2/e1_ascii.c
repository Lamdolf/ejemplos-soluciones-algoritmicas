#include <stdio.h>

//VERSIÓN EN ASCII DE EJERCICIO 1

int main(){

    char frase[99];
    int aux = 1;

    printf("Ingrese una frase: ");
    scanf("%[^\n]s", frase); //scanf que captura espacios entre frases

    for(int i=0; frase[i]!= '\0'; i++){

        //Si se la variable auxiliar es 1 (true), significa que se encontró
        //un espacio, por lo que se revisa de que caracter se trata
        if (aux){

            //Si es una letra ñ, su mayúscula la procede en ascii
            if(frase[i] == 164){
                printf("%c", frase[i]+1);
            }

            //Si es una letra minúscula, se imprime la letra mayúscula (32
            //unidades atrás en ascii)
            else if(frase[i] < 123 && frase[i] > 92){
                printf("%c", frase[i]-32);
            }

            //Cualquier otro caracter especial se imprime simplemente
            else{
                printf("%c", frase[i]);
            }

            aux = 0;
        }

        //Si se encuentra un espacio, se cambia el booleano a "true"
        else if (frase[i] == 32){
            printf(" ");
            aux = 1;
        }

        //Si no se encontró un espacio, ni el caracter actual es antecedido
        //por un espacio, se imprime el caracter actual
        else{
            printf("%c", frase[i]);
        }
    }

    return 0;
}
